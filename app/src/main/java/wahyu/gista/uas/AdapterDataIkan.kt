package wahyu.gista.uas

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_katalogikan.*

class AdapterDataIkan(val dataIkan: List<HashMap<String,String>>,
                      val IkanActivity: IkanActivity) : //new
    RecyclerView.Adapter<AdapterDataIkan.HolderDataIkan>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataIkan {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_ikan, p0, false)
        return HolderDataIkan(v)
    }

    override fun getItemCount(): Int {
        return dataIkan.size
    }

    override fun onBindViewHolder(p0: AdapterDataIkan.HolderDataIkan, p1: Int) {
        val data = dataIkan.get(p1)
        p0.txIkan.setText(data.get("nim"))
        p0.txNamaIkan.setText(data.get("nama"))
        p0.txJenis.setText(data.get("nama_prodi"))
        p0.txJK.setText(data.get("kelamin"))

        //beginNew
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230, 245, 240)
        )
        else p0.cLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            val pos = IkanActivity.daftarJenis.indexOf(data.get("nama_prodi"))
            IkanActivity.spinJenisIkan.setSelection(pos)
            val pos1 =  IkanActivity.daftarKelamin.indexOf(data.get("kelamin"))
            IkanActivity.spinKelamin.setSelection(pos1)
            IkanActivity.edIkan.setText(data.get("nim"))
            IkanActivity.edNamaIkan.setText(data.get("nama"))
            Picasso.get().load(data.get("url")).into( IkanActivity.imUpload)
        })
        //endNew
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }


    class HolderDataIkan(v: View) : RecyclerView.ViewHolder(v) {
        val txIkan = v.findViewById<TextView>(R.id.txIKAN)
        val txNamaIkan = v.findViewById<TextView>(R.id.txNamaIkan)
        val txJenis = v.findViewById<TextView>(R.id.txJenis)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val txJK = v.findViewById<TextView>(R.id.txJK)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout) //new
    }
}