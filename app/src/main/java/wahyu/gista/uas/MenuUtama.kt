package wahyu.gista.uas

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_menu_utama.*

class MenuUtama: AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_utama)

        btnIkan.setOnClickListener(this)
        btnJenis.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnIkan ->{
                var intentIkan = Intent(this, AdapterDataIkan::class.java)
                startActivity(intentIkan)
            }
            R.id.btnJenis ->{
                var intentJenis = Intent(this, JenisActivity::class.java)
                startActivity(intentJenis)
            }
        }
    }
}